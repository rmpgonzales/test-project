package com.example.demo.Model.Entity;

import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Entity
@Getter
@Setter
@FieldNameConstants
@Table(name="testview", schema="testschema")
public class TestViewEntity {
    @Id
    private BigInteger id;

    @Column
    private String name;

    @Column 
    private String author;

    @Column(name = "createddate")
    private LocalDate createdDate;
}
