package com.example.demo.Model.DTO;

import java.math.BigInteger;
import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestDTO {
    private BigInteger id;
    private String name;
    private String author;
    private LocalDate createdDate;
}
