package com.example.demo.Model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseModel<T> {
    private List<T> content;
    private Integer pageNumber;
    private Integer totalPages;
    private Integer totalItems;
}
