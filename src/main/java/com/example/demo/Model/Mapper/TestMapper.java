package com.example.demo.Model.Mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.Common.Mapper;
import com.example.demo.Model.DTO.TestDTO;
import com.example.demo.Model.Entity.TestModel;

@Component
public class TestMapper implements Mapper<TestModel, TestDTO>{

    @Override
    public TestDTO entityToDTO(TestModel entityModel) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TestModel dTOToEntity(TestDTO dto) {
        TestModel model = new TestModel();
        model.setId(dto.getId());
        model.setAuthor(dto.getAuthor());
        model.setCreatedDate(dto.getCreatedDate());
        model.setName(dto.getName());
        return model;
    }

    @Override
    public List<TestDTO> entityListToDTOList(List<TestModel> entityModelList) {
        List<TestDTO> dtoList = new ArrayList<TestDTO>();
        for(TestModel entity: entityModelList){
            TestDTO dto = new TestDTO();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setAuthor(entity.getAuthor());
            dto.setCreatedDate(entity.getCreatedDate());

            dtoList.add(dto);
        }
        return dtoList;
    }
}
