package com.example.demo.Model.Mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.Common.Mapper;
import com.example.demo.Model.DTO.TestViewDTO;
import com.example.demo.Model.Entity.TestViewEntity;

@Component
public class TestViewMapper implements Mapper<TestViewEntity, TestViewDTO>{

    @Override
    public TestViewDTO entityToDTO(TestViewEntity entityModel) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TestViewEntity dTOToEntity(TestViewDTO DTO) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<TestViewDTO> entityListToDTOList(List<TestViewEntity> entityModelList) {
        List<TestViewDTO> dtoList = new ArrayList<TestViewDTO>();
        for(TestViewEntity entity: entityModelList){
            TestViewDTO dto = new TestViewDTO();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setAuthor(entity.getAuthor());
            dto.setCreatedDate(entity.getCreatedDate());

            dtoList.add(dto);
        }
        return dtoList;
    }
    
}
