package com.example.demo.Service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.example.demo.Common.DtoBuilder;
import com.example.demo.Common.Response;
import com.example.demo.Model.RequestModel;
import com.example.demo.Model.ResponseModel;
import com.example.demo.Model.DTO.TestDTO;
import com.example.demo.Model.DTO.TestViewDTO;
import com.example.demo.Model.Entity.TestModel;
import com.example.demo.Model.Entity.TestViewEntity;
import com.example.demo.Model.Mapper.TestMapper;
import com.example.demo.Model.Mapper.TestViewMapper;
import com.example.demo.Service.ServiceHelper.TestServiceHelper;

@Service
public class TestService extends DtoBuilder {

    @Autowired
    private TestViewMapper testMapper;

    @Autowired
    private TestServiceHelper testServiceHelper;

    public ResponseModel<TestViewDTO> getData(RequestModel model, LocalDate dateFrom, LocalDate dateTo, String category,
            String categoryValue) {

        testServiceHelper.validateParameter();

        Page<TestViewEntity> testPage = testServiceHelper.findAll(model, dateFrom, dateTo, category, categoryValue);

        return generateResponse(new TestViewDTO(), testPage,
                testMapper.entityListToDTOList(testPage.getContent()));
    }

    public ResponseModel<TestViewDTO> checkData(TestDTO requestBody, RequestModel model) {
        //testServiceHelper.validateParameter(testMapper.dTOToEntity(requestBody));
        
        return null;
    }

}
