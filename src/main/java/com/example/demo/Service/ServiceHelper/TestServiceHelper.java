package com.example.demo.Service.ServiceHelper;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.tomcat.util.http.parser.HttpParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.Common.SpecUtil;
import com.example.demo.DAO.TestDAO;
import com.example.demo.DAO.TestViewDao;
import com.example.demo.Model.RequestModel;
import com.example.demo.Model.ResponseModel;
import com.example.demo.Model.DTO.TestDTO;
import com.example.demo.Model.Entity.TestModel;
import com.example.demo.Model.Entity.TestViewEntity;

@Component
public class TestServiceHelper {

    @Autowired
    private SpecUtil specUtil;

    @Autowired
    private TestViewDao testViewDao;

    @Autowired
    private TestDAO testDao;

    public void validateParameter() {
        // validation here
    }

    public void validateParameter(TestModel requestBody) {
        TestModel probe = new TestModel();
        probe.setAuthor(requestBody.getAuthor());
        probe.setCreatedDate(requestBody.getCreatedDate());
        probe.setName(requestBody.getName());
        probe.setId(requestBody.getId());
        Example<TestModel> example = Example.of(probe);

        // Optional<TestModel> record = testDao.findOne(example);
        // testDao.save(requestBody);
        // if (record.isEmpty()) {
        //     throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Does not exist");
        // }
    }

    public Page<TestViewEntity> findAll(RequestModel model, LocalDate dateFrom, LocalDate dateTo, String category,
            String categoryValue) {
        PageRequest page = PageRequest.of(model.getPageNumber(), model.getPageSize(),
                Sort.by(Direction.valueOf(model.getSortDirection()), model.getSortBy()));

        Specification<TestViewEntity> spec = specUtil.isNotNull(new TestViewEntity(), TestViewEntity.Fields.id);
        Specification<TestViewEntity> specCreatedDate = specUtil.greaterThanOrEqualTo(new TestViewEntity(), dateFrom,
                TestModel.Fields.createdDate);
        Specification<TestViewEntity> specAuthor = specUtil.likeFilter(new TestViewEntity(), category, categoryValue);

        // if (Objects.nonNull(dateFrom)) {
        //     spec = spec.and(specCreatedDate);
        // }

        // if (Objects.nonNull(category) && Objects.nonNull(categoryValue)) {
        //     spec = spec.and(specAuthor);
        // }

        spec = spec.and(specCreatedDate).and(specAuthor);
    
        return testViewDao.findAll(spec, page);
    }

    public void save(TestModel body){

        testDao.save(body);
    }
}
