package com.example.demo.Common;

import java.time.LocalDate;
import java.util.Objects;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class SpecUtil {

    public <T> Specification<T> isNotNull(T entity, String field){
        if(StringUtils.hasText(field)){
            return (root, cq, cb) -> cb.isNotNull(root.get(field));
        }
        return null;
    }

    public <T> Specification<T> greaterThanOrEqualTo(T entity, LocalDate dateFrom, String field){
        if(Objects.nonNull(dateFrom)){
            return (root, cq, cb) -> cb.greaterThanOrEqualTo(root.get(field), dateFrom);
        }
        return null;
    }

    public <T> Specification<T> likeFilter(T entity, String field, String value){
        if(StringUtils.hasText(field) && StringUtils.hasText(value)){
            return (root, cq, cb) -> cb.like(root.get(field), "%" + value + "%");
        }
        return null;
    }
}
