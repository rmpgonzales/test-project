package com.example.demo.Common;

public enum Constants {
    TESTENUMONE(1,"Test message");

    private Integer code;
    private String message;

    Constants(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public Integer getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }
}
