package com.example.demo.Common;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.example.demo.Model.ResponseModel;

@Component
public class DtoBuilder {
    public <T, U> ResponseModel<T> generateResponse(T dtoClass, Page<U> page, List<T> dtoList) {
        ResponseModel<T> response = new ResponseModel<>();
        response.setContent(dtoList);
        response.setTotalPages(page.getTotalPages());
        response.setPageNumber(page.getNumber());
        response.setTotalItems(page.getNumberOfElements());
        return response;
    }
}
