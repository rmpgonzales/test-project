package com.example.demo.Common;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Response {
    public <T> ResponseEntity<Map<String,Object>> generateResponse(T content, HttpStatus status, String message){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", content);
        map.put("status", status.value());
        map.put("message", message);
        map.put("timestamp", LocalDateTime.now().toString());
        return new ResponseEntity<Map<String,Object>>(map,status);
    }

    public <T> ResponseEntity<Map<String,Object>> generateResponse(T content, HttpStatus status, List<String> message){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", content);
        map.put("status", status.value());
        map.put("message", message);
        map.put("timestamp", LocalDateTime.now().toString());
        return new ResponseEntity<Map<String,Object>>(map,status);
    }
}
