package com.example.demo.Common;

import java.util.List;

public interface Mapper<T,U> {
    public U entityToDTO (T entityModel);
    public T dTOToEntity(U DTO);
    public List<U> entityListToDTOList(List<T> entityModelList);
}
