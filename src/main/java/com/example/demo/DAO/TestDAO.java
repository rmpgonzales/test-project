package com.example.demo.DAO;

import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.demo.Model.Entity.TestModel;

public interface TestDAO extends JpaRepository<TestModel, BigInteger>, JpaSpecificationExecutor<TestModel>{
    public Page<TestModel> findByNameContainsAndId(String name, BigInteger id, Pageable page);
}
