package com.example.demo.DAO;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.demo.Model.Entity.TestViewEntity;

public interface TestViewDao extends JpaRepository<TestViewEntity, BigInteger>, JpaSpecificationExecutor<TestViewEntity>{
    
}
