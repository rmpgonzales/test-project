package com.example.demo.Controller;

import java.time.LocalDate;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.Common.Constants;
import com.example.demo.Common.Response;
import com.example.demo.Model.RequestModel;
import com.example.demo.Model.DTO.TestDTO;
import com.example.demo.Service.TestService;

@Controller
@ResponseBody
@Validated
@RequestMapping("/testmain")
public class TestController extends Response {

    @Autowired
    private TestService testService;

    @GetMapping("/test")
    public ResponseEntity<Map<String, Object>> getTest(@Valid RequestModel requestModel, BindingResult bindingResult, ModelMap model,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
            @RequestParam(required = false) String category, @RequestParam(required = false) String categoryValue) {
        return generateResponse(
                testService.getData(requestModel, dateFrom, dateTo, category, categoryValue),
                HttpStatus.OK,
                "test message");
    }

    @PutMapping("/check")
    public ResponseEntity<Map<String,Object>> checkTest(@Valid @RequestBody TestDTO requestBody, @Valid RequestModel requestModel){
        return generateResponse(testService.checkData(requestBody, requestModel), HttpStatus.OK, "nice");
    }

}
